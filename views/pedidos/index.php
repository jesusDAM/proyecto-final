<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pedidos';

?>
<div class="pedidos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir un nuevo pedido', ['../mine/pedidos'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'producto',
                'value' => 'producto.nombre'
            ],
            [
                'attribute' => 'proveedor',
                'value' => 'proveedor.nombre'
            ],
            [
                'attribute' => 'fecha',
                'format' => ['date', 'php:d/m/Y']
            ],
            'precio',
            'unidades',
        ],
    ]); ?>


</div>
