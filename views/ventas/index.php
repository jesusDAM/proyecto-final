<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ventas';

?>
<div class="ventas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir una nueva venta', ['../mine/ventas'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'empleado',
                'value' => 'empleado.nombre'
            ],
            [
                'attribute' => 'fecha',
                'format' => ['date', 'php:d/m/Y']
            ],
            'precio',
            'descuento',
        ],
    ]); ?>


</div>
