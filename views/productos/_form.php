<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Departamentos;
use yii\helpers\ArrayHelper;

$departamentos=Departamentos::find()->all();
$listData=ArrayHelper::map($departamentos, 'id','nombre');

/* @var $this yii\web\View */
/* @var $model app\models\Productos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unidades')->textInput() ?>

    <?= $form->field($model, 'precio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_departamento')->dropDownList($listData, ['prompt' => 'Selecciona uno...'])->label('Departamento') ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
