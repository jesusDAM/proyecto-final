<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos Ventas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-ventas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Productos Ventas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_producto',
            'id_venta',
            'cantidad',
            'precio_unidad',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
