<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DireccionProveedores */

$this->title = 'Create Direccion Proveedores';
$this->params['breadcrumbs'][] = ['label' => 'Direccion Proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="direccion-proveedores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
