<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Direccion Proveedores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="direccion-proveedores-index">
    <br><br><br><br><br>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id_proveedor',
            [
                'attribute' => 'nombre',
                'value' => 'proveedor.nombre'
            ],
            [
                'attribute' => 'apellidos',
                'value' => 'proveedor.apellidos'
            ],
            'direccion',
        ],
    ]); ?>


</div>
