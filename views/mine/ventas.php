<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\Empleados;
use yii\helpers\ArrayHelper;

$empleados=Empleados::find()->all();
$listData=ArrayHelper::map($empleados,'id','nombre');

?>

<h1>Añadir una nueva venta</h1>

<?php $form = ActiveForm::begin(['options' => ['id' => 'ventas', 'enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'fecha')->input('date') ?>

<?= $form->field($model, 'precio') ?>

<?= $form->field($model, 'descuento') ?>

<?= $form->field($model, 'id_empleado')->dropDownList($listData, ['prompt' => 'Selecciona uno...']) ?>

<?= Html::submitButton('Añadir', ['class' => 'btn btn-success']) ?>

<?php ActiveForm::end() ?>