<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\Productos;
use app\models\Proveedores;
use yii\helpers\ArrayHelper;

$productos=Productos::find()->all();
$listData_1=ArrayHelper::map($productos,'id','nombre');

$proveedores=Proveedores::find()->all();
$listData_2=ArrayHelper::map($proveedores,'id','nombre');

?>

<h1>Hacer un nuevo pedido</h1>

<?php $form = ActiveForm::begin(['options' => ['id' => 'pedidos', 'enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'fecha')->input('date') ?>

<?= $form->field($model, 'precio') ?>

<?= $form->field($model, 'unidades') ?>

<?= $form->field($model, 'id_producto')->dropDownList($listData_1, ['prompt' => 'Selecciona uno...']) ?>

<?= $form->field($model, 'id_proveedor')->dropDownList($listData_2, ['prompt' => 'Selecciona uno...']) ?>

<?= Html::submitButton('Añadir', ['class' => 'btn btn-success']) ?>

<?php ActiveForm::end() ?>