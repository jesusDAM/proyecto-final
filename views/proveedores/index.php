<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proveedores';

?>
<div class="proveedores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'nombre',
            'apellidos',
            [
                'attribute' => 'direccion',
                'value' => 'direccionProveedores.direccion'
            ],
            [
                'attribute' => 'telefono',
                'value' => 'telefonoProveedores.telefono'
            ],
        ],
    ]); ?>


</div>
