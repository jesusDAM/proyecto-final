<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Marcas';

?>
<div class="marcas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'nombre',
            [
                'attribute' => 'proveedor',
                'value' => 'proveedor.nombre'
            ],
        ],
    ]); ?>


</div>
