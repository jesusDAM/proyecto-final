<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos Compras';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-compras-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Productos Compras', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_producto',
            'id_compra',
            'cantidad',
            'precio_unidad',
            'id',
            //'id_producto_compra',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
