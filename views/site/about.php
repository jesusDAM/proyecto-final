<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Sobre nosotros';

?>
<div class="site-about">
    
    <h1><?= Html::encode($this->title) ?></h1>

    <p align="justify">
        Somos una empresa de consultoría que se dedica a crear aplicaciones web para franquicias de diferentes tipos.<br>
    </p>
    
    <p align="justify">
        En este caso, Mercamanager es una aplicación enfocada a aquellos franquiciados que acaban de abrir un supermercado y necesitan un software para gestionar su supermercado de una manera fácil.
    </p>
    
    <p align="justify">
        Nosotros trabajamos para cualquier franquicia de supermercado, ya sea Mercadona, Carrefour, Lidl... No nos importa el tamaño que tenga el supermercado, ni el número de empleados del mismo, ni su ubicación. Sólo queremos ayudar a los franquiciados a facilitar las gestiones del día a día de su supermercado.
    </p>
    
    <p align="justify">
        Es por eso por el que ofrecemos esta aplicación web, que se puede utilizar en diferentes plataformas, ya sea en un PC (Windows, LInux o MacOS), en un teléfono mévil (iOS o Android) o en cualquier otro dispositivo. Lo único que necesitas es un navegador web con conexión a Internet para utilizar la aplicación.
    </p>
    
    <p align="justify">
        Con Mercamanager, gestionar tu supermercado será una tarea fácil y cómoda.
    </p>
    
</div>

<?= Html::img('@web/images/logo.png', ['alt' => 'Mercamanager']) ?>