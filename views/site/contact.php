<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contacto';

?>
<div class="site-contact">
    
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Gracias por contactarnos, le responderemos lo más pronto posible.
        </div>

        <p>
            Tenga en cuenta que si activa Yii Debugger, podrá ver el mensaje en el panel de mensajes.
            <?php if (Yii::$app->mailer->useFileTransport): ?>
                Debido a que la aplicación está en desarrollo, el correo no se envía
                pero se guarda como un archivo en <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
                Por favor, configure el <code>useFileTransport</code> propiedad del componente de la aplicación <code>mail</code>
                y póngalo en falso para activar el envío de correos.
            <?php endif; ?>
        </p>

    <?php else: ?>

        <p>
            Si tiene alguna duda o alguna pregunta, por favor, rellene el siguiente formulario para contactarnos.
            Gracias.
        </p>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($model, 'Nombre')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'Correo') ?>

                    <?= $form->field($model, 'Asunto') ?>

                    <?= $form->field($model, 'Mensaje')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'Verificación')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php endif; ?>
</div>
