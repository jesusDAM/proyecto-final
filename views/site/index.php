<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'MercaManager';
?>

<div class="site-index">

    <div class="jumbotron">
        
        <h1>Bienvenidos a MercaManager</h1>
        
        <?= Html::img('@web/images/logo.png', ['alt' => 'MercaManager']) ?>

        <p class="lead">Una aplicación web para gestionar su supermercado de forma fácil y cómoda</p>

        <p><a class="btn btn-lg btn-success" href="site/about">Más información acerca de nosotros</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2 align="center">Fácil gestión de su supermercado</h2>

                <p align="justify">Gestione a sus empleados y proveedores, gestione los departamentos de su supermercado, haga inventario, compuebe si faltan
                existencias, haga pedidos a sus proveedores y gestione las compras y ventas realizadas en el supermercado de una forma fácil, rápida y
                cómoda. Con MercaManager, puede realizar todas las tareas de un supermercado sin complicaciones.</p>

            </div>
            <div class="col-lg-4">
                <h2 align="center">Haga sus gestiones en cualquier dispositivo</h2>

                <p align="justify">MercaManager, al ser una aplicación web, no necesita nada más que un navegador web para poder ser utilizada. Es decir, puede utilizar
                nuestra aplicación en cualquier dispositivo con conexión a Internet, da igual que sea un teléfono móvil, una Tablet o un ordenador. No
                importa el sistema operativo, MercaManager se puede utilizar en cualquier parte, en cualquier momento.</p>

            </div>
            <div class="col-lg-4">
                <h2 align="center">Completamente gratuita</h2>

                <p align="justify">Al ser una aplicación web que puede ser utlizada en cualquier sitio, MercaManager puede ser utlizada de forma totalmente gratuita, es
                decir, no le vamos a cobrar ninguna tarifa para que usted pueda disfrutar de nuestras funcionalidades. Tampoco verá su experiencia
                entorpecida por bloques de publicidad, por lo que nuestra apliación está hecha para un uso y una experiencia sin complejos.</p>

            </div>
        </div>

    </div>
    
    <div class="jumbotron">
        
        <h2>Regístrese en nuestra aplicación y comience a disfrutar de sus ventajas</h2>

        <p><a class="btn btn-lg btn-success" href="site/about">Iniciar sesión</a></p>
        
    </div>
    
</div>
