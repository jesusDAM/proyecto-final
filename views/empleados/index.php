<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Empleados';

?>
<div class="empleados-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'nombre',
            'apellidos',
            'email:email',
            'clave',
            'tipo',
            [
                'attribute' => 'cargo',
                'value' => 'cargo.tipo'
            ],
            [
                'attribute' => 'direccion',
                'value' => 'direccionEmpleados.direccion'
            ],
            [
                'attribute' => 'telefono',
                'value' => 'telefonoEmpleados.telefono'
            ],
        ],
    ]); ?>


</div>
