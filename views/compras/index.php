<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Compras';

?>
<div class="compras-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir una nueva compra', ['../mine/compras'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'empleado',
                'value' => 'empleado.nombre'
            ],
            [
                'attribute' => 'fecha',
                'format' => ['date', 'php:d/m/Y']
            ],
            'precio',
        ],
    ]); ?>


</div>
