<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Telefono Empleados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telefono-empleados-index">
    <br><br><br><br><br>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id_empleado',
            [
                'attribute' => 'nombre',
                'value' => 'empleado.nombre'
            ],
            [
                'attribute' => 'apellidos',
                'value' => 'empleado.apellidos'
            ],
            'telefono',
        ],
    ]); ?>


</div>
