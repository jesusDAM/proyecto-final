<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos_compras".
 *
 * @property int|null $id_producto
 * @property int|null $id_compra
 * @property int|null $cantidad
 * @property float|null $precio_unidad
 *
 * @property Compras $compra
 * @property Productos $producto
 */
class ProductosCompras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos_compras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_producto', 'id_compra', 'cantidad'], 'integer'],
            [['precio_unidad'], 'number'],
            [['id_compra'], 'exist', 'skipOnError' => true, 'targetClass' => Compras::className(), 'targetAttribute' => ['id_compra' => 'id']],
            [['id_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['id_producto' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_producto' => 'Id Producto',
            'id_compra' => 'Id Compra',
            'cantidad' => 'Cantidad',
            'precio_unidad' => 'Precio Unidad',
        ];
    }

    /**
     * Gets query for [[Compra]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompra()
    {
        return $this->hasOne(Compras::className(), ['id' => 'id_compra']);
    }

    /**
     * Gets query for [[Producto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Productos::className(), ['id' => 'id_producto']);
    }
}
