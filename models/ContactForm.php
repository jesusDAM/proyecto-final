<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $Nombre;
    public $Correo;
    public $Asunto;
    public $Mensaje;
    public $Verificación;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['Nombre', 'Correo', 'Asunto', 'Mensaje'], 'required'],
            // email has to be a valid email address
            ['Correo', 'email'],
            // verifyCode needs to be entered correctly
            ['Verificación', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'Verificación' => 'Codigo de verificación',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        $content = "<p>Correo: " . $this->Correo . "</p>";
        $content = "<p>Nombre: " . $this->Nombre . "</p>";
        $content = "<p>Asunto: " . $this->Asunto . "</p>";
        $content = "<p>Mensaje: " . $this->Mensaje . "</p>";
        if ($this->validate()) {
            Yii::$app->mailer->compose("@app/mail/layouts/html", ["content" => $content])
                ->setTo($email)
                ->setFrom([$this->Correo => $this->Nombre])
                ->setSubject($this->Asunto)
                ->setTextBody($this->Mensaje)
                ->send();

            return true;
        } else {
            return false;
        }
    }
}
