<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleados".
 *
 * @property int $id
 * @property string|null $email
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $clave
 * @property string|null $tipo
 * @property int|null $id_cargo
 *
 * @property Compras[] $compras
 * @property DireccionEmpleados $direccionEmpleados
 * @property Cargos $cargo
 * @property TelefonoEmpleados $telefonoEmpleados
 * @property Ventas[] $ventas
 */
class Empleados extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_cargo'], 'integer'],
            [['email', 'apellidos', 'direccion'], 'string', 'max' => 50],
            [['nombre', 'tipo'], 'string', 'max' => 20],
            [['clave'], 'string', 'max' => 8],
            [['telefono'], 'string', 'max' => 9],
            [['id_cargo'], 'exist', 'skipOnError' => true, 'targetClass' => Cargos::className(), 'targetAttribute' => ['id_cargo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'clave' => 'Clave',
            'tipo' => 'Tipo',
            'cargo' => 'Cargo',
            'direccion' => 'Dirección',
            'telefono' => 'Teléfono',
        ];
    }

    /**
     * Gets query for [[Compras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->hasMany(Compras::className(), ['id_empleado' => 'id']);
    }

    /**
     * Gets query for [[DireccionEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDireccionEmpleados()
    {
        return $this->hasOne(DireccionEmpleados::className(), ['id_empleado' => 'id'], ['direccion' => 'direccion']);
    }

    /**
     * Gets query for [[Cargo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCargo()
    {
        return $this->hasOne(Cargos::className(), ['id' => 'id_cargo'], ['tipo' => 'tipo']);
    }

    /**
     * Gets query for [[TelefonoEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonoEmpleados()
    {
        return $this->hasOne(TelefonoEmpleados::className(), ['id_empleado' => 'id'], ['telefono' => 'telefono']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::className(), ['id_empleado' => 'id']);
    }

    public function getAuthKey(): string {
        return $this->email;
        
    }

    public function getId() {
        return $this->id;
        
    }

    public function validateAuthKey($email): bool {
        return $this->email === $email;
        
    }

    public static function findIdentity($id) {
        return self::findOne($id);
        
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new \yii\base\NotSupportedException();
        
    }
    
    public static function findByUsername($nombre){
        return self::findOne(['nombre'=>$nombre]);
    }
    
    public function validatePassword($clave){
        return $this->clave === $clave;
    }

}
