<?php

namespace app\models;
use yii\db\ActiveRecord;

class ComprasForm extends ActiveRecord
{
    public static function tableName(){
        return 'compras';
    }
    
    public function rules(){
        return [
           [ [ 'fecha', 'precio', 'id_empleado' ], 'required' ],
           [ 'precio', 'double' ],
        ];
    }
    
    public function attributeLabels(){
        return[
            'id_empleado' => 'Empleado',
        ];
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

