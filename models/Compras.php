<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compras".
 *
 * @property int $id
 * @property string|null $fecha
 * @property float|null $precio
 * @property int|null $id_empleado
 *
 * @property Empleados $empleado
 * @property ProductosCompras[] $productosCompras
 */
class Compras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['precio'], 'number'],
            [['empleado'], 'string', 'max' => 20],
            [['id_empleado'], 'integer'],
            [['id_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['id_empleado' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'precio' => 'Precio',
            'empleado' => 'Empleado',
        ];
    }

    /**
     * Gets query for [[Empleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['id' => 'id_empleado']);
    }

    /**
     * Gets query for [[ProductosCompras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosCompras()
    {
        return $this->hasMany(ProductosCompras::className(), ['id_compra' => 'id']);
    }
}
