<?php

namespace app\models;
use yii\db\ActiveRecord;

class PedidosForm extends ActiveRecord
{
    public static function tableName(){
        return 'pedidos';
    }
    
    public function rules(){
        return [
           [ [ 'fecha', 'precio', 'unidades', 'id_producto', 'id_proveedor' ], 'required' ],
           [ 'precio', 'double' ],
           [ 'unidades', 'integer' ],
        ];
    }
    
    public function attributeLabels(){
        return[
            'id_producto' => 'Producto',
            'id_proveedor' => 'Proveedor',
        ];
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

