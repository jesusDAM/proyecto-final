<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marcas".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $id_proveedor
 *
 * @property Proveedores $proveedor
 * @property ProductosMarcas[] $productosMarcas
 */
class Marcas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marcas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_proveedor'], 'integer'],
            [['nombre', 'proveedor'], 'string', 'max' => 50],
            [['id_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['id_proveedor' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'proveedor' => 'Proveedor',
        ];
    }

    /**
     * Gets query for [[Proveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedor()
    {
        return $this->hasOne(Proveedores::className(), ['id' => 'id_proveedor']);
    }

    /**
     * Gets query for [[ProductosMarcas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosMarcas()
    {
        return $this->hasMany(ProductosMarcas::className(), ['id_marca' => 'id']);
    }
}
