<?php

namespace app\models;
use yii\db\ActiveRecord;

class VentasForm extends ActiveRecord
{
    public static function tableName(){
        return 'ventas';
    }
    
    public function rules(){
        return [
           [ [ 'fecha', 'precio', 'descuento', 'id_empleado' ], 'required' ],
           [ 'precio', 'double' ],
           [ 'descuento', 'integer' ],
        ];
    }
    
    public function attributeLabels(){
        return[
            'id_empleado' => 'Empleado',
        ];
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

