<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\ComprasForm;
use app\models\VentasForm;
use app\models\PedidosForm;

class MineController extends Controller
{
    public function actionCompras()
    {
        $model = new ComprasForm();
        
        if ( $model->load(Yii::$app->request->post()) ){
            if ( $model->save() ){
                return $this->redirect('../compras/index');
            }
        }
        
        return $this->render('compras', compact('model'));
    }
    
    public function actionVentas()
    {
        $model = new VentasForm();
        
        if ( $model->load(Yii::$app->request->post()) ){
            if ( $model->save() ){
                return $this->redirect('../ventas/index');
            }
        }
        
        return $this->render('ventas', compact('model'));
    }
    
    public function actionPedidos()
    {
        $model = new PedidosForm();
        
        if ( $model->load(Yii::$app->request->post()) ){
            if ( $model->save() ){
                return $this->redirect('../pedidos/index');
            }
        }
        
        return $this->render('pedidos', compact('model'));
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

